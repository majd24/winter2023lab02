public class MethodsTest {
	public static void main(String[] args) {
		int x = 5;
		
		System.out.println(x);
		
		methodNoInputNoReturn();
		
		System.out.println(x);
		
		System.out.println(x);
		
		methodOneInputNoReturn(x + 10);
		
		System.out.println(x);
		
		methodTwoInputNoReturn(1, 2.5);
		
		int number = methodNoInputReturnInt();
		
		System.out.println(number);
		
		double resultOfSquareRoot =  sumSquareRoot(9, 5);
		
		System.out.println(resultOfSquareRoot);
		
		String s1 = "java";
		
		String s2 = "programming";
		
		System.out.println(s1.length());
		
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		
		SecondClass object = new SecondClass();
		int resultOfAddTwo = object.addTwo(50);
		System.out.println(resultOfAddTwo);
		
	}
	
	public static void methodNoInputNoReturn() {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
		
		
	}
	
	public static void methodOneInputNoReturn(int variable) {
		variable -= 5;
		System.out.println("Inside the method one input no return");
		System.out.println(variable);
		
	}
	
	public static void methodTwoInputNoReturn(int x, double y) {
		System.out.println("x: " + x + " y: " + y);
		
		
		
	}
	public static int methodNoInputReturnInt() {
		return 5;
		
	}
	
	public static double sumSquareRoot(int varX, int varY) {
		int adding = varX + varY;
		double squareRoot = Math.sqrt(adding);
		return squareRoot;
		
		
	}

}

/*
scratch:.

There are some libraries that are built in such as "java.lang.math". However some of the libraries 
need to be imported such as "java.util.Scanner".

The "Math.sqrt()" is an example of a static method. "Math" is the name of the class and "sqrt()"
is the method that we are calling. However things change when it comes to non-static method
(instance) such as "object.nextInt()". In here we have a class called "Scanner" and has an instance method
"nextInt()". To call the method we need to first write "Scanner object = new Scanner(System.in)"
where "Scanner is the name of the class (Notice that the name of the class starts always with a capital letter)
and "object" refers to an example of a type "Scanner"
then we will use that example before the instance method that we are calling "object.nextInt()".

Now the "String" class contains an instance method "length()" for example BUT in java we don't need
to write "String object = new String()".



*/