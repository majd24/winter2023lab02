import java.util.Scanner;
public class PartThree {
	public static void main(String[] args) {
		Scanner object = new Scanner(System.in);
		System.out.println("Please enter two values and I will show you the addition, subtraction, multiplication and division of them");
		double firstValue = object.nextDouble();
		double secondValue = object.nextDouble();
		System.out.println("adding " + firstValue + " and " + secondValue + " is equal to " + Calculator.sum(firstValue, secondValue));
		System.out.println("subtracting " + firstValue + " from " + secondValue + " is equal to " + Calculator.subtract(firstValue, secondValue));
		
		Calculator secondObject = new Calculator();
		System.out.println("multiplying " + firstValue + " and " + secondValue + " is equal to " + secondObject.multiplication(firstValue, secondValue));
		
		System.out.println("dividing " + firstValue + " by " + secondValue + " is equal to " + secondObject.division(firstValue, secondValue));
	}
}